let resultdata = JSON.parse(sessionStorage.getItem("data"));

let graphicElevation = document.getElementById("elevation");
let imgElev = graphicElevation.toDataURL("image/png");

let graphicAzimuth = document.getElementById("azimuth");
let imgAzim = graphicAzimuth.toDataURL("image/png");

const latSide = (lat) => (Number(lat) >= 0) ? "N" : "S";

const lonSide = (lon) => (Number(lon) >= 0) ? "E" : "O";

const generatePDF = () => {

	const pdfdoc = new jsPDF({
    	orientation: "portrait",
	    unit: "mm",
	    format: "a4"
	 });

	 /**	Printing data 	**/

	//Writing title
	pdfdoc.setFontSize(24);
	pdfdoc.setFont("Arial", "bold");
	pdfdoc.text("RESULTADOS", 77, 20);

	//Writing parameters
	pdfdoc.setFontSize(16);
	pdfdoc.setFont("Arial", "bold");
	pdfdoc.text("Estación terrestre:", 20, 35);
	pdfdoc.setFontSize(12);
	pdfdoc.setFont("Arial", "normal");
	pdfdoc.text(`Localidad: ${resultdata.earthstation.location}`, 30, 43);
	pdfdoc.text(`Latitud: ${Math.abs(resultdata.earthstation.latitude)} °${latSide(resultdata.earthstation.latitude)}`, 30, 50);
	pdfdoc.text(`Longitud: ${Math.abs(resultdata.earthstation.longitude)} °${lonSide(resultdata.earthstation.longitude)}`, 30, 57);


	pdfdoc.setFontSize(16);
	pdfdoc.setFont("Arial", "bold");
	pdfdoc.text("Datos del satélite:", 20, 70);
	pdfdoc.setFontSize(12);
	pdfdoc.setFont("Arial", "normal");
	pdfdoc.text(`Nombre: ${resultdata.satellite.name}`, 30, 78);
	pdfdoc.text(`Longitud: ${Math.abs(resultdata.satellite.longitude)} °${lonSide(resultdata.satellite.longitude)}`, 30, 85);

	//Writing results
	pdfdoc.setFontSize(16);
	pdfdoc.setFont("Arial", "bold");
	pdfdoc.text("Parámetros encontrados:", 20, 98);
	pdfdoc.setFontSize(12);
	pdfdoc.setFont("Arial", "normal");
	pdfdoc.text(`Elevación: ${resultdata.params.elevation}°`, 30, 106);
	pdfdoc.text(`Distancia: ${resultdata.params.distance} km`, 30, 113);
	pdfdoc.text(`Azimut: ${resultdata.params.azimuth}°`, 30, 120);
	pdfdoc.text(`Ángulo Gamma: ${resultdata.params.gamma} rad`, 30, 127);
	pdfdoc.text(`Parámetro phi: ${resultdata.params.phi} rad`, 30, 134);

	/*	end print data */

	//Drawing images
	pdfdoc.addImage(imgElev, 40, 140, 60, 60);
	pdfdoc.addImage(imgAzim, 110, 140, 60, 60);

	//Generating uri
	pdfdoc.output("dataurlnewwindow");

};
