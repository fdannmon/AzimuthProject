<?php

class Azimuth
{
	function __construct($stLoc, $stLat, $stLon, $satName, $satLon)
	{
		$this->stLocation = $stLoc;
		$this->stLatitude = $stLat;
		$this->stLongitude = $stLon;
		$this->satName = $satName;
		$this->satLongitude = $satLongitude;
	}


	/* Getters and setters */

	public function get_station_location()
	{
		return $this->stLocation;
	}

	public function set_station_location($stLoc)
	{
		$this->stLocation = $stLoc;
	}

	public function get_station_latitude()
	{
		return $this->stLatitude;
	}

	public function set_station_location($stLat)
	{
		$this->stLatitude = $stLat;
	}

	public function get_station_longitude()
	{
		return $this->stLongitude;
	}

	public function set_station_longitude($stLon)
	{
		$this->stLongitude = $stLon;
	}

	public function get_satellite_name()
	{
		return $this->satName;
	}

	public function set_satellite_name($satName)
	{
		$this->satName = $satName;
	}

	public function get_satellite_longitude()
	{
		return $this->satLongitude;
	}

	public function set_satellite_longitude($satLon)
	{
		$this->satLongitude = $satLon;
	}

}


?>