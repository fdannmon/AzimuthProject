/*
  ** Quadrant function **
*/

const quadrantDraw = (ctx, center, radius) => {

  /*  The center attribute must be an object
      with params called centerX and centerY  */

  //Starting
  ctx.beginPath();
  ctx.moveTo(center.centerX, center.centerY);
  ctx.lineTo(center.centerX + radius, center.centerY);
  ctx.arc(center.centerX, center.centerY, radius, 0,(-1)*Math.PI/2, true);
  ctx.lineTo(center.centerX, center.centerY);
  ctx.stroke();
  ctx.closePath();

  ctx.font = "17px bold";
  ctx.fillText("0°", center.centerX + radius + 5,  center.centerY);
  ctx.fillText("90°", center.centerX - 5, center.centerY - radius - 5);
};
/* end quad function */


/*
  ** Elevation graphic function **
*/
const elevationGraphic = (ctx, center, radius, elAngleRad) => {

  quadrantDraw(ctx, center, radius);

  /*
    Drawing the elevation angle
  */

  let degElevation = elAngleRad;
  let radElevation = parseFloat((degElevation*Math.PI/180).toFixed(3));

  let imgAnthena = new Image();
  imgAnthena.src = "https://cdn1.iconfinder.com/data/icons/journalism-and-press/80/Journalism_media_press-13-512.png";

  let imgSatellite = new Image();
  imgSatellite.src = "http://image.flaticon.com/icons/png/512/178/178537.png";

  const imgAnt = {
    x: 40,
    y: 40
  };
  const imgSat = {
    x: 40,
    y: 40
  };

  //Drawing images
  imgSatellite.addEventListener("load", () => {
    ctx.drawImage(imgSatellite,
      center.centerX + radius*Math.cos(radElevation) - imgSat.x/2,
      center.centerY - radius*Math.sin(radElevation) - imgSat.y/2,
      imgSat.x,
      imgSat.y);
  });

  imgAnthena.addEventListener("load", () => {
    ctx.drawImage(imgAnthena,
      center.centerX - imgAnt.x/2,
      center.centerY - imgAnt.y/2,
      imgAnt.x,
      imgAnt.y);
  });

  //Printing angle
  ctx.fillText(`${degElevation}°`,
    center.centerX + radius*Math.cos(radElevation) + imgSat.x/2,
    center.centerY - radius*Math.sin(radElevation) - imgSat.y/5
  );

  //Drawing direction line in red
  ctx.beginPath();
  ctx.strokeStyle = "rgb(180, 45, 45)";
  ctx.lineWidth = 3;
  ctx.moveTo(center.centerX, center.centerY);
  ctx.lineTo(center.centerX + radius*Math.cos(radElevation), center.centerY - radius*Math.sin(radElevation));
  ctx.stroke();
  ctx.closePath();
};
/* end elevation graphic function */


//Definning canvas
const elevCanvas = document.getElementById("elevation");
const ctx = elevCanvas.getContext("2d");

let elevAngle = JSON.parse(sessionStorage.getItem("data")).params.elevation;


const center = {
  centerX: 60,
  centerY: 175
};
const radius = 125;

elevationGraphic(ctx, center, radius, elevAngle);
ctx.font = "bold 14px sans-serif";
ctx.fillText(
  `Elevación: ${elevAngle}°`,
  10,
  240
);
