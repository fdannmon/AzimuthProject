/*
  * Defining function for drawing pointed lines
  */

const PointedLine = (ctx, start, end) => {
  const partitX = end.x - start.x;
  const partitY = end.y - start.y;
  for(let i=0; i<15; i++)
  {
    if(i%2==0)
    {
      ctx.beginPath();
      ctx.moveTo(start.x + i*partitX/15, start.y + i*partitY/15);
      ctx.lineTo(start.x + (i+1)*partitX/15, start.y + (i+1)*partitY/15);
      ctx.stroke();
      ctx.closePath();
    }
  }
};

/*  Drawing cardinal points  */
const cardPoint = angle => {
  if(angle == 0)
  {
    return "N";
  } else if(angle == 90) {
    return "E";
  } else if (angle == 180) {
    return "S";
  } else if(angle == 270) {
    return "O";
  }
  return `${angle}째`;
};



/*
  ** AzimuthGraphic prototype **
  */
const AzimuthGraphic = function(ctx, center, radius, azimuthAngle) {
  this.startPoint = (-1)*Math.PI/2;
  this.context = ctx;
  this.center = center; //object with x and y params
  this.radius = radius; //pixels
  this.azimuth = azimuthAngle; //degrees
  this.azimuthRads = this.azimuth*Math.PI/180;
};


AzimuthGraphic.prototype.drawBorder = function() {
  //Drawing pointed circle
  for(let i=0; i<60; i++)
    {
      if(i%2 == 0)
        {
          this.context.beginPath();
          this.context.arc(
            this.center.x,
            this.center.y,
            this.radius,
            this.startPoint + i*Math.PI/30,
            this.startPoint + (i+1)*Math.PI/30,
            false
          );
          this.context.stroke();
          this.context.closePath();
        }
    }

  //Drawing lines
  for(let i=0; i<8; i++)
  {
    let angle = ((this.startPoint + Math.PI/2 + i*Math.PI/4)*180/Math.PI).toFixed();
    PointedLine(
      this.context,
      {
        x: this.center.x,
        y: this.center.y
      },
      {
        x: this.center.x + this.radius*Math.cos(this.startPoint + i*Math.PI/4),
        y: this.center.y + this.radius*Math.sin(this.startPoint + i*Math.PI/4)
      }
    );
    if(angle%90 == 0)
    {
      this.context.font = "bold 14px sans-serif";
    } else {
      this.context.font = "normal 12px sans-serif";
    }
    this.context.fillText(
      `${cardPoint(angle)}`,
      angle<180 ? this.center.x + (this.radius+1)*Math.cos(this.startPoint + i*Math.PI/4): this.center.x + (this.radius+10)*Math.cos(this.startPoint + i*Math.PI/4) - 10,
      (angle>=90 && angle<=270) ? this.center.y + (this.radius+15)*Math.sin(this.startPoint + i*Math.PI/4) + 2: this.center.y + this.radius*Math.sin(this.startPoint + i*Math.PI/4) - 3
    );
  }

};

AzimuthGraphic.prototype.drawAzimuthLine = function() {

  this.context.beginPath();
  this.context.strokeStyle = "rgb(180, 45, 45)";
  this.context.lineWidth = 3;
  this.context.moveTo(this.center.x, this.center.y);
  this.context.lineTo(
    this.center.x + this.radius*Math.cos(this.startPoint + this.azimuthRads),
    this.center.y + this.radius*Math.sin(this.startPoint + this.azimuthRads)
  );
  this.context.stroke();
  this.context.closePath();
};

AzimuthGraphic.prototype.drawGraphics = function() {
  const imgAnthena = new Image();
  const imgSatellite = new Image();

  imgAnthena.src = "https://cdn2.iconfinder.com/data/icons/hotel-and-restaurant-solid-icons-vol-1/64/013-128.png";
  imgSatellite.src = "http://image.flaticon.com/icons/png/512/178/178537.png";

  const dimImgs = {
    x: 40,
    y: 40
  };

  imgAnthena.addEventListener("load", () => {
    this.context.drawImage(
      imgAnthena,
      this.center.x - dimImgs.x/2,
      this.center.y - dimImgs.y/2,
      dimImgs.x,
      dimImgs.y
    );
  });

  imgSatellite.addEventListener("load", () => {
    this.context.drawImage(
      imgSatellite,
      this.center.x + this.radius*Math.sin(this.azimuthRads) - dimImgs.x/2,
      this.center.y - this.radius*Math.cos(this.azimuthRads) - dimImgs.y/2,
      dimImgs.x,
      dimImgs.y
    );
  });

  this.context.font = "bold 14px sans-serif";

  this.context.fillText(
    `Azimut: ${this.azimuth}째`,
    10,
    240
  );

}

/* end AzimuthGraphic prototype */



/*
  * Drawing canvas and angles *
  */

const azCanvas = document.getElementById("azimuth");
let azimAngle = JSON.parse(sessionStorage.getItem("data")).params.azimuth; //Getting azimuth angle

const azimGraph = new AzimuthGraphic(
  azCanvas.getContext("2d"),
  {
    x: 125,
    y: 115
  },
  80,
  azimAngle
);

azimGraph.drawBorder();
azimGraph.drawGraphics();
azimGraph.drawAzimuthLine();
