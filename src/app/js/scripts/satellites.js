const satellites = [
  {
    name: "Intelsat 18",
  	longitude: 180.0
  },
  {
  	name: "Eutelsat 172A",
  	longitude: 172.0
  },
  {
  	name: "Intelsat 8",
  	longitude: 169.0
  },
  {
  	name: "Intelsat 19",
  	longitude: 166.0
  },
  {
  	name: "Superbird B2",
  	longitude: 162.0
  },
  {
  	name: "Optus D1",
  	longitude: 160.0
  },
  {
  	name: "Optus C1 / D3",
  	longitude: 156.0
  },
  {
  	name: "Optus D3 / D3",
  	longitude: 156.0
  },
  {
  	name: "JCSAT 2A",
  	longitude: 154.0
  },
  {
  	name: "Optus D2",
  	longitude: 152.0
  },
  {
  	name: "Superbird C2",
  	longitude: 144.0
  },
  {
  	name: "Express AM 5",
  	longitude: 140.0
  },
  {
  	name: "Telstar 18",
  	longitude: 138.0
  },
  {
  	name: "Apstar 6",
  	longitude: 134.0
  },
  {
  	name: "Vinasat 2",
  	longitude: 132.0
  },
  {
  	name: "JCSAT 5A / Vinasat 1",
  	longitude: 132.0
  },
  {
  	name: "JCSAT 3A",
  	longitude: 128.0
  },
  {
  	name: "ChinaSat 6A",
  	longitude: 125.0
  },
  {
  	name: "JCSAT 4B",
  	longitude: 124.0
  },
  {
  	name: "AsiaSat 4",
  	longitude: 122.2
  },
  {
  	name: "Thaicom 4",
  	longitude: 119.5
  },
  {
  	name: "Telkom 2",
  	longitude: 118.0
  },
  {
  	name: "KoreaSat 6 / ABS 7",
  	longitude: 116.0
  },
  {
  	name: "ChinaSat 6B",
  	longitude: 115.5
  },
  {
  	name: "KoreaSat 5 / Palapa D",
  	longitude: 113.0
  },
  {
  	name: "ChinaSat 10",
  	longitude: 110.5
  },
  {
  	name: "BSAT 110R/3A/3C / N-sat 110",
  	longitude: 110.0
  },
  {
  	name: "NSS 11 / SES 7",
  	longitude: 108.2
  },
  {
  	name: "Telkom 1",
  	longitude: 108.0
  },
  {
  	name: "AsiaSat 7",
  	longitude: 105.5
  },
  {
  	name: "Express AM 3",
  	longitude: 103.0
  },
  {
  	name: "AsiaSat 5",
  	longitude: 100.5
  },
  {
  	name: "Express AM 33",
  	longitude: 96.5
  },
  {
  	name: "NSS 6/8",
  	longitude: 95.0
  },
  {
  	name: "Insat 3A / Insat 4B",
  	longitude: 93.5
  },
  {
  	name: "ChinaSat 9",
  	longitude: 92.2
  },
  {
  	name: "MeaSat 3",
  	longitude: 91.5
  },
  {
  	name: "MeaSat 3a",
  	longitude: 91.4
  },
  {
  	name: "Yamal 201/300K",
  	longitude: 90.0
  },
  {
  	name: "ST 2",
  	longitude: 88.0
  },
  {
  	name: "ChinaSat 5A",
  	longitude: 87.5
  },
  {
  	name: "KazSat 2",
  	longitude: 86.5
  },
  {
  	name: "Intelsat 15",
  	longitude: 85.2
  },
  {
  	name: "Horizons 2",
  	longitude: 85.0
  },
  {
  	name: "Insat 4A",
  	longitude: 83.0
  },
  {
  	name: "Thaicom 5/6",
  	longitude: 78.5
  },
  {
  	name: "Apstar 7",
  	longitude: 76.5
  },
  {
  	name: "ABS 2",
  	longitude: 75.0
  },
  {
    name: "Insat 3C/4CR",
    longitude: 74.0
  },
  {
    name: "EutelSat 70B",
    longitude: 70.5
  },
  {
    name: "IntelSat 20",
    longitude: 68.5
  },
  {
    name: "IntelSat 17",
    longitude: 66.0
  },
  {
    name: "IntelSat 906",
    longitude: 64.2
  },
  {
    name: "IntelSat 902",
    longitude: 62.0
  },
  {
    name: "IntelSat 904",
    longitude: 60.0
  },
  {
    name: "NSS 12",
    longitude: 57.0
  },
  {
    name: "Express AT1",
    longitude: 56.0
  },
  {
    name: "Yamal 402 / G-Sat 8",
    longitude: 55.0
  },
  {
    name: "Express AM 22",
    longitude: 53.0
  },
  {
    name: "Y1A",
    longitude: 52.5
  },
  {
    name: "BelinterSat",
    longitude: 51.5
  },
  {
    name: "Yamal 202",
    longitude: 49.0
  },
  {
    name: "AfghanSat 1",
    longitude: 48.0
  },
  {
    name: "IntelSat 10",
    longitude: 47.5
  },
  {
    name: "AzerSpace 1 / AfricaSat 1A",
    longitude: 46.0
  },
  {
    name: "IntelSat 12",
    longitude: 45.0
  },
  {
    name: "TurkSat 2A / 3A / 4A",
    longitude: 42.0
  },
  {
    name: "Hellas Sat 2",
    longitude: 39.0
  },
  {
    name: "PakSat 1",
    longitude: 38.0
  },
  {
    name: "EutelSat 36A / 36B",
    longitude: 36.0
  },
  {
    name: "EutelSat 33B",
    longitude: 33.0
  },
  {
    name: "IntelSat 28",
    longitude: 32.9
  },
  {
    name: "Astra 1G",
    longitude: 31.5
  },
  {
    name: "EutelSat 31A",
    longitude: 30.8
  },
  {
    name: "ArabSat 5B",
    longitude: 30.5
  },
  {
    name: "ArabSat 5A",
    longitude: 30.5
  },
  {
    name: "EutelSat 28A",
    longitude: 28.5
  },
  {
    name: "Astra 2A/E/F",
    longitude: 28.2
  },
  {
    name: "Badr 4/5/6",
    longitude: 26.0
  },
  {
    name: "EutelSat 25B / Es'hail 1",
    longitude: 25.5
  },
  {
    name: "Astra 3B",
    longitude: 23.5
  },
  {
    name: "EutelSat 21B",
    longitude: 21.5
  },
  {
    name: "ArabSat 5C",
    longitude: 20.0
  },
  {
    name: "Astra 1KR/L/M/N",
    longitude: 19.2
  },
  {
    name: "EutelSat 16A",
    longitude: 16.0
  },
  {
    name: "EutelSat HotBird 13B/C/D",
    longitude: 13.0
  },
  {
    name: "EutelSat 10A",
    longitude: 10.0
  },
  {
    name: "EuroBird 9A / Ka-Sat 9A",
    longitude: 9.0
  },
  {
    name: "EuroBird 9A / Ka-Sat",
    longitude: 9.0
  },
  {
    name: "EutelSat 7A",
    longitude: 7.0
  },
  {
    name: "SES 5",
    longitude: 5.0
  },
  {
    name: "Astra 4A",
    longitude: 4.8
  },
  {
    name: "EutelSat 3A",
    longitude: 3.3
  },
  {
    name: "EutelSat 3D",
    longitude: 3.1
  },
  {
    name: "Rascom QAF 1R",
    longitude: 2.8
  },
  {
    name: "BulgariaSat 1",
    longitude: 1.9
  },
  {
    name: "Thor 5/6",
    longitude: -0.8
  },
  {
    name: "IntelSat 10-02",
    longitude: -1.0
  },
  {
    name: "Amos 2/3",
    longitude: -4.0
  },
  {
    name: "EutelSat 5 West A",
    longitude: -5.0
  },
  {
    name: "NileSat 102/201",
    longitude: -7.0
  },
  {
    name: "EutelSat 8 West C",
    longitude: -7.3
  },
  {
    name: "EutelSat 7 West A",
    longitude: -7.3
  },
  {
    name: "EutelSat 8 West A",
    longitude: -8.0
  },
  {
    name: "Express AM 44",
    longitude: -11.0
  },
  {
    name: "EutelSat 12 West A",
    longitude: -12.5 
  },
  {
    name: "Telstar 12",
    longitude: -15.0
  },
  {
    name: "IntelSat 901",
    longitude: -18.0
  },
  {
    name: "NSS 7",
    longitude: -20.0
  },
  {
    name: "SES 4",
    longitude: -22.0
  },
  {
    name: "IntelSat 905",
    longitude: -24.5
  },
  {
    name: "IntelSat 907",
    longitude: -27.5
  },
  {
    name: "HispaSat 1D/1E",
    longitude: -30.0
  },
  {
    name: "IntelSat 25",
    longitude: -31.5
  },
  {
    name: "IntelSat 903",
    longitude: -34.5
  },
  {
    name: "NSS 10",
    longitude: -37.5
  },
  {
    name: "Telstar 11N",
    longitude: -37.6
  },
  {
    name: "SES 6",
    longitude: -40.5
  },
  {
    name: "IntelSat 9/11",
    longitude: -43.1
  },
  {
    name: "EchoStar 15",
    longitude: -44.9
  },
  {
    name: "IntelSat 14",
    longitude: -45.0
  },
  {
    name: "NSS 806",
    longitude: -47.5
  },
  {
    name: "IntelSat 1R",
    longitude: -50.0
  },
	{
		name: "Amazonas 4A",
		longitude: -51.0
	},
	{
		name: "IntelSat 23",
		longitude: -53.0
	},
	{
		name: "IntelSat 805 / Galaxy 11",
		longitude: -55.5
	},
	{
		name: "IntelSat 21",
		longitude: -58.0
	},
	{
		name: "Amazonas 2/3",
		longitude: -61.0
	},
	{
		name: "EchoStar 12 / 16",
		longitude: -61.5
	},
	{
		name: "TelStar 14R",
		longitude: -63.0
	},
	{
		name: "Star One C1",
		longitude: -65.0
	},
	{
		name: "AMC 4",
		longitude: -67.0
	},
	{
		name: "Star One C2",
		longitude: -70.0
	},
	{
		name: "AMC 6",
		longitude: -72.0
	},
	{
		name: "Nimiq 5",
		longitude: -72.7
	},
	{
		name: "Star One C3",
		longitude: -75.0
	},
	{
		name: "EchoStar 8 / QuetzSat 1",
		longitude: -77.0
	},
	{
		name: "Simón Bolívar",
		longitude: -78.0
	},
	{
		name: "Nimiq 4",
		longitude: -82.0
	},
	{
		name: "AMC 9",
		longitude: -83.0
	},
	{
		name: "HispaSat 1C",
		longitude: -83.8
	},
	{
		name: "BrasilSat B4",
		longitude: -84.0
	},
	{
		name: "AMC 16",
		longitude: -85.0
	},
	{
		name: "XM 3",
		longitude: -85.1
	},
	{
		name: "Sirius XM 5",
		longitude: -85.2
	},
	{
		name: "SES 2",
		longitude: -87.0
	},
	{
		name: "Galaxy 28",
		longitude: -89.0
	},
	{
		name: "Galaxy 17 / Nimiq 6",
		longitude: -91.0
	},
	{
		name: "Galaxy 25",
		longitude: -93.1
	},
	{
		name: "Galaxy 3C / Spaceway 3",
		longitude: -95.0
	},
	{
		name: "Sirius FM 5",
		longitude: -96.0
	},
	{
		name: "Galaxy 19",
		longitude: -97.0
	},
	{
		name: "Galaxy 16",
		longitude: -99.0
	},
	{
		name: "Spaceway 2 / DirecTV 11",
		longitude: -99.2
	},
	{
		name: "DirecTV 4S",
		longitude: -101.0
	},
	{
		name: "DirecTV 8 / SES 1",
		longitude: -101.0
	},
	{
		name: "Spaceway 1 & DirecTV 10/12",
		longitude: -102.8
	},
	{
		name: "AMC 1",
		longitude: -103.0
	},
	{
		name: "AMC 15/18",
		longitude: -105.0
	},
	{
		name: "Anik G1 / F1R",
		longitude: -107.3
	},
	{
		name: "DirecTV 5",
		longitude: -109.8
	},
	{
		name: "EchoStar 10/11",
		longitude: -110.0
	},
	{
		name: "Anik F2",
		longitude: -111.1
	},
	{
		name: "EutelSat 113 West A",
		longitude: -113.0
	},
	{
		name: "MexSat Bicentenario",
		longitude: -114.5
	},
	{
		name: "EutelSat 115 West A",
		longitude: -114.9
	},
	{
		name: "XM 4",
		longitude: -115.0
	},
	{
		name: "EutelSat 117 West A",
		longitude: -116.8
	},
	{
		name: "Anik F3 / D-TV 7S / EchoStar 14",
		longitude: -119.0
	},
	{
		name: "EchoStar 9 / Galaxy 23",
		longitude: -121.0
	},
	{
		name: "Galaxy 18",
		longitude: -123.0
	},
	{
		name: "Galaxy 14 / AMC 21",
		longitude: -125.0
	},
	{
		name: "Galaxy 13 / Horizons 1",
		longitude: -127.0
	},
	{
		name: "Ciel 2 / Galaxy 12",
		longitude: -129.0
	},
	{
		name: "AMC 11",
		longitude: -131.0
	},
	{
		name: "Galaxy 15",
		longitude: -133.2
	},
	{
		name: "AMC 10",
		longitude: -135.0
	},
	{
		name: "AMC 7",
		longitude: -137.0
	},
	{
		name: "AMC 8",
		longitude: -139.0
	},
	{
		name: "NSS 9",
		longitude: -177.0
	},
];

export {satellites};
