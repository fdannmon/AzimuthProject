import {satellites} from "./satellites.js";
import {Azimuth} from "./Azimuth.js";

const formAzimuth = new Vue({

	/*	Getting the form	*/
	el: "#formdata",
	/*	end	*/

	/*	Properties of the form	*/
	data: {
		stLocation: "",
		stLatitude: "",
		stLongitude: "",
		satellites: satellites,
		choosenSat: "",
	},
	/*	end	*/

	/*	Methods to be used	*/
	methods: {
		//Validate only numbers
		onlyNumbers: function(e){
			let theEvent = e || window.event;
			let key = theEvent.keyCode || theEvent.which;
			key = String.fromCharCode(key);
			const regex = /[0-9]|\.|\-|[\b]|\t/;
			if(!key.match(regex))  //it could also be !regex.test(key)
			{
			  theEvent.returnValue = false;
			  if(!theEvent.defaultPrevented)
			  {
			    theEvent.preventDefault();
			  }
			}
		},
		//Getting East or West
		lonSide: function(lon){
			return ( Number(lon) >= 0 || isNaN(Number(lon)) ) ? "E" : "O";
		},
		//Show the satellite longitude
		showSatLon: function(){
			return !isNaN(Math.abs(this.satLongitude)) ? Math.abs(this.satLongitude) : '';
		},
		//Working data after submit
		workData: function(e){
			let stloc = document.getElementById("stLocation").value;
			let stlat = Number(document.getElementById("stLat").value);
			let stlon = Number(document.getElementById("stLon").value);
			let satname = document.getElementById("satName").value;
			let satlon = Number(document.getElementById("satLon").value);
			//Define Azimuth data
			let azimuthResults = new Azimuth(stloc, stlat, stlon, satname, satlon);
			//Working with data
			let resultsObj = {
				 earthstation: {
					location: azimuthResults.locationStation,
					latitude: azimuthResults.latitudeStation,
					longitude: azimuthResults.longitudeStation,
				},
				satellite: {
					name: azimuthResults.locationSatellite,
					longitude: azimuthResults.longitudeSatellite
				},
				params: {
					gamma: azimuthResults.getGamma(),
					elevation: azimuthResults.getElevation(),
					distance: azimuthResults.getDistance(),
					phi: azimuthResults.getPhi(),
					azimuth: azimuthResults.getAzimuth()
				}
			};
			//Store into session storage
			let _results = JSON.stringify(resultsObj);
			sessionStorage.setItem("data", _results);
		},
	},
	/*	end */

	/*	Computed properties	*/
	computed: {
		correctData: function(){
			return !(
				( Number(this.stLatitude) <= 90 && Number(this.stLatitude) >= -90 ) &&
				( Number(this.stLongitude) <= 180 && Number(this.stLongitude) >= -180 )
			);
		},
		satName: function(){
			return this.choosenSat.name;
		},
		satLongitude: function(){
			return this.choosenSat.longitude;
		}
	},
	/*	end	*/

});
