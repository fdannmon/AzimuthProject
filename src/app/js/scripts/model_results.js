const resultsObject = JSON.parse(sessionStorage.getItem("data"));

const resultsData = new Vue({
  el: "#results",
  data: {
    results: resultsObject,
  },
  methods: {
    //Getting North or South
		latSide: function(lat){
			return (Number(lat) >= 0) ? "N" : "S";
		},
    //Getting East or West
		lonSide: function(lon){
			return (Number(lon) >= 0) ? "E" : "O";
		},
  },
});
