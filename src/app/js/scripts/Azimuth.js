`use strict`

export class Azimuth
{

    constructor(locSt, latSt, lonSt, locSat, lonSat) {
        this.locationStation = locSt;
        this.latitudeStation = latSt;
        this.longitudeStation = lonSt;
        this.locationSatellite = locSat;
        this.longitudeSatellite = lonSat;
        this.eRadius = 6378.14; // in Km
        this.seRadius = 42164.57; // in Km
    }


    /* Methods to calculate data */

    getGamma() {
        /*
        * This method returns gamma parameter in radians.
        To calculate it we need station latitude and longitude
        and the satellite longitude in radians, too.
        */
        let gamValue = Math.acos(Math.cos(this.latitudeStation*Math.PI/180)*Math.cos(Math.abs(this.longitudeSatellite - this.longitudeStation)*Math.PI/180))
        return parseFloat(gamValue.toFixed(4));
    }

    getElevation() {
        /*
        * This method returns the elevation angle in degrees.
        * Gamma parameter must be in radians
        */
        let elValue = (Math.atan((6.61075 - Math.cos(this.getGamma()))/Math.sin(this.getGamma())) - this.getGamma())*180/Math.PI;
        return parseFloat(elValue.toFixed(2));
    }

    getDistance() {
        /*
        * This method returns the distance between the earth station and the satellite.
        * Distance will return in Km, and we need the gamma parameter to calculate it.
        */
        let distValue = this.seRadius*Math.sqrt(1 + Math.pow(this.eRadius/this.seRadius, 2)
        - 2*(this.eRadius/this.seRadius*Math.cos(this.getGamma())));
        return parseFloat(distValue.toFixed(3));
    }

    getPhi() {
        /*
        * This method returns phi parameter in radians, which
        could help us to calculate azimut angle.
        */
        let phiValue = Math.atan(Math.tan(Math.abs(this.longitudeSatellite - this.longitudeStation)*Math.PI/180)/Math.sin(this.latitudeStation*Math.PI/180));
        return parseFloat(phiValue.toFixed(4));
    }

    getAzimuth() {
        /*
        * This function returns the azimut angle, in degrees °N.
        We'll need the phi parameter.
        */
        let azValue = 0;
        if(this.longitudeSatellite < this.longitudeStation && this.latitudeStation > 0) {
            azValue = 180 + this.getPhi()*180/Math.PI;
        }
        else if(this.longitudeSatellite > this.longitudeStation && this.latitudeStation > 0) {
            azValue = 180 - this.getPhi()*180/Math.PI;
        }
        else if(this.longitudeSatellite > this.longitudeStation && this.latitudeStation < 0) {
            azValue = this.getPhi()*180/Math.PI;
        }
        else if(this.longitudeSatellite < this.longitudeStation && this.latitudeStation < 0) {
            azValue = 360 - this.getPhi()*180/Math.PI;
        }

        return parseFloat(azValue.toFixed(2));
    }
    /* end */

}
